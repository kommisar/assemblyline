import math
import mock
import threading
import time


class MockSleep:
    """
    An object to replace the sleep function to force time based code to move in
    lockstep with the testing routine.
    """

    def __init__(self):
        self.lock = threading.Lock()
        self.condition = threading.Condition(self.lock)
        self.sleeping = 0
        self.sleep = time.sleep
        self.slept = 0
        self._iterations = 0

    def __call__(self, duration):
        self.slept += 1
        self.sleeping += 1
        start = time.time()
        while time.time() - start < duration:
            self._iterations += 1
            with self.lock:
                self.condition.wait(0.1)
        self.sleeping -= 1

    def wait(self):
        start = self._iterations
        while start == self._iterations:
            self.wake()

    def wake(self):
        with self.lock:
            self.condition.notifyAll()
        self.sleep(0.001)


class MockTime:
    """
    Can be used to replace time.time and time.sleep for testing modules that
    have time based properties.
    """

    def __init__(self):
        self.sleep = MockSleep()
        self.time = mock.Mock()
        self._sleep_patch = mock.patch('time.sleep', self.sleep)
        self._time_patch = mock.patch('time.time', self.time)

    def __enter__(self):
        self.time.return_value = 0
        self._sleep_patch.start()
        self._time_patch.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._sleep_patch.stop()
        self._time_patch.stop()

    def advance(self, span, interval=0.1):
        steps = max(1, int(math.ceil(span/interval)))
        for _ in range(steps):
            self.time.return_value += interval
            self.sleep.wake()
