
from assemblyline.common.versioning import git_repo_revision
import os.path
import pytest


def test_not_repo():
    with pytest.raises(ValueError):
        git_repo_revision('/')


def test_is_repo():
    rev = git_repo_revision(os.path.abspath(os.path.dirname(__file__)))
    assert type(rev) == str
    assert ' ' not in rev
    assert '\n' not in rev
    assert len(rev) == 40
