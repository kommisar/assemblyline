
from assemblyline.common import timeout
from assemblyline.test import MockTime

import unittest
import time
import mock


class TestSubprocessTimer(unittest.TestCase):

    def test_direct_call(self):
        """In the following, we initialize a timer and and use it multiple times"""
        with MockTime() as time:
            timer = timeout.SubprocessTimer(3)

            def run_mock_sp_timer(span):
                job = mock.Mock()
                timer.run(job)
                time.sleep.wait()

                # Wait for the job to 'finish' or timeout
                time.advance(span)

                # Check the actions of the thread
                if span > 3:
                    assert timer.timed_out
                    job.kill.assert_called()
                else:
                    assert not timer.timed_out

                return timer.has_timed_out()

            self.assertTrue(run_mock_sp_timer(5))
            self.assertFalse(run_mock_sp_timer(1))
            self.assertTrue(run_mock_sp_timer(5))
            timer.close()

    def test_context(self):
        """In the following, we use a timer wrapped into a 'with' statement"""
        # print("\n\n'With statement' timer kills processes running more than 2 seconds...")
        # print("\n-->> Executing commandL: \"sleep 4\"")
        with MockTime() as time:
            stime = etime = time.time()
            ret_val = None
            with self.assertRaises(timeout.TimeoutException):
                with timeout.SubprocessTimer(2) as new_ST:
                    job = mock.Mock()
                    proc = new_ST.run(job)
                    while not job.kill.called:
                        time.advance(0.1)

                    ret_val = proc.poll()
                    etime = time.time()

            self.assertLess(etime - stime, 4)
            self.assertGreater(etime - stime, 1.5)
            self.assertNotEqual(ret_val, 0)


def return_after(value, timeout):
    time.sleep(timeout)
    return value


class TestTimeout(unittest.TestCase):
    def test_notimeout(self):
        self.assertEqual(timeout.timeout(return_after, (10, 1), timeout_duration=2), 10)
        self.assertEqual(timeout.timeout(return_after, ("abc", 1), timeout_duration=2), "abc")

    def test_timeout(self):
        with self.assertRaises(BaseException):
            timeout.timeout(return_after, (10, 10), timeout_duration=2)


class TestAlarm(unittest.TestCase):
    def test_noalarm(self):
        with timeout.alarm_clock(1):
            # We won't wait long enough trigger the alarm
            time.sleep(0.5)
        # And the alarm is disabled after the block ends
        time.sleep(1)

    def test_alarm(self):
        with self.assertRaises(timeout.TimeoutException):
            with timeout.alarm_clock(1):
                time.sleep(10)
            self.fail()
