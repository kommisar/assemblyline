
import unittest
from assemblyline.common.path import splitpath, isUNCLegal


class TestSplitPath(unittest.TestCase):
    def test_split(self):
        self.assertEqual(splitpath(''), [])
        self.assertEqual(splitpath('/'), [])
        self.assertEqual(splitpath('hello'), ['hello'])
        self.assertEqual(splitpath('/hello'), ['hello'])
        self.assertEqual(splitpath('//hello'), ['hello'])
        self.assertEqual(splitpath('hello/'), ['hello'])
        self.assertEqual(splitpath('/hello/world/'), ['hello', 'world'])
        self.assertEqual(splitpath('hello/world'), ['hello', 'world'])
        self.assertEqual(splitpath('./hello/world/'), ['.', 'hello', 'world'])
        self.assertEqual(splitpath('../../hello/world/there'), ['..', '..', 'hello', 'world', 'there'])


    def test_UNC(self):
        """Test a couple illegal paths"""
        self.assertFalse(isUNCLegal(""))

        # Some unix paths and files
        self.assertFalse(isUNCLegal("/"))
        self.assertFalse(isUNCLegal("./.vimrc"))
        self.assertFalse(isUNCLegal("./.vimrc"))
        self.assertFalse(isUNCLegal("~/notes/readme.txt"))

        # Some URLS
        self.assertFalse(isUNCLegal('http://google.com'))
        self.assertFalse(isUNCLegal('ftp://fileserve.io'))

        # Actual paths
        self.assertTrue(isUNCLegal('\\\\some-server\\directory'))
