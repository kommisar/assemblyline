#!/usr/bin/env python
from __future__ import absolute_import

import argparse

from assemblyline.al.common import log
from assemblyline.al.core.agents import HostAgent


def main():
    log.init_logging('hostagent')
    agent = HostAgent()

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--sysprep', action='store_true')
    group.add_argument('--updateandexit', action='store_true')
    group.add_argument('--register', action='store_true')
    args = parser.parse_args()

    if args.sysprep or args.updateandexit:
        result = agent.sysprep()
        print 'SysPrep: %s' % str(result)
        exit(0)
    elif args.register:
        result = agent.register_host()
        print "Registration Result: %s" % str(result)
        exit(0)

    agent.serve_forever()


if __name__ == '__main__':
    main()
