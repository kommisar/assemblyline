#!/usr/bin/env python


def install(alsi=None):
    alsi.append_line_if_doesnt_exist("/etc/hosts", "%s    datastore.al" % alsi.get_ipaddress())
    alsi.info("Patched /etc/hosts to add datastore.al.")

if __name__ == '__main__':
    from assemblyline.al.install import SiteInstaller
    installer = SiteInstaller()
    install(installer)
